# Replace Tool for Level Designers in Unity

This repository contain Replace Tool for Level Designers. You can check how it was created!

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/08/26/replace-tool-for-level-designers/

Enjoy!

---

# How to use it?

This repository contains an example of how you can build a replace tool for Unity.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/replace-tool/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

For more visit my blog: https://www.patrykgalach.com
